#!/bin/bash

file=log.csv

IFS=";"

while read -r Port Opened Type REST; do
    echo "----------------------"
    echo "Port and Protocol: $Port"
    echo "Opened or closed: $Opened"
    echo "Type: $Type"
    echo "----------------------"
done < "$file"
